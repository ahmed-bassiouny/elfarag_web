<?php
require_once "check_auth.php";
// Include config file
require_once "config.php";


$sql = 'SELECT * FROM trips_for_manager ORDER BY id DESC';
$result = mysqli_query($link, $sql);



$resStr = "";
if (mysqli_num_rows($result) > 0) {
  // output data of each row
  while ($item = mysqli_fetch_assoc($result)) {
    $resStr .= '<form name="reg-frm" action="" method="POST">';
    $resStr .= '<tr>';
    $resStr .= '<td hidden>' . '<input name="id" value = "' . $item['id'] . '">' . '</input></td>';
    $resStr .= '<td><a href="map.html?lat=' . $item["from_lat"] . '&lng=' . $item["from_lng"] . '">' . $item['from_text'] . '</a></td>';
    $resStr .= '<td><a href="map.html?lat=' . $item["to_lat"] . '&lng=' . $item["to_lng"] . '">' . $item['to_text'] . '</a></td>';
    if ($item['is_open'] == 1) {
      $resStr .= '<td><button class = "bg-success text-white"  type="submit"  name="save">مفتوح</td>';
    } else {
      $resStr .= '<td>مغلقة</td>';
    }
    $resStr .= '<td>' . $item['created_at'] . '</td>';
    $resStr .= '<td>' . $item['note'] . '</td>';
    $resStr .= '<td>' . $item['admin_note'] . '</td>';
    $resStr .= '<td><a href="user_details.php?id=' . $item['user_firebase_key'] . '">تفاصيل المستخدم<a> <br/> <a style="color:red" onclick="openForm(' . $item['id'] . ')">كتابة ملاحظة</a></td>';
    $resStr .= '</tr>';
    $resStr .= '</form>';
  }
}




if (isset($_POST['save']) && isset($_POST['id'])) {
  $id = $_POST['id'];
  $query1 = "UPDATE trips_for_manager SET is_open=0 WHERE id='$id'";
  if (mysqli_query($link, $query1)) {
    header("Refresh:0");
  } else {
    echo mysqli_error($link);
  }
}

if (isset($_POST['save_note'])) {
  $id = $_POST['trip_id'];
  $note = $_POST['admin_note'];

  $query1 = "UPDATE trips_for_manager SET admin_note = '$note' WHERE id='$id'";
  if (mysqli_query($link, $query1)) {
    header("Refresh:0");
  } else {
    echo mysqli_error($link);
  }
}
?>
<!DOCTYPE html>
<html lang="ar" dir="rtl">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>الرحلات التى ترسل للادارة</title>

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <style>
    /* Button used to open the contact form - fixed at the bottom of the page */
    .open-button {
      background-color: #555;
      color: white;
      padding: 16px 20px;
      border: none;
      cursor: pointer;
      opacity: 0.8;
      position: fixed;
      bottom: 23px;
      right: 28px;
      width: 280px;
    }

    /* The popup form - hidden by default */
    .form-popup {
      display: none;
      position: fixed;
      bottom: 0;
      right: 15px;
      border: 3px solid #f1f1f1;
      z-index: 9;
    }

    /* Add styles to the form container */
    .form-container {
      max-width: 300px;
      padding: 10px;
      background-color: white;
    }

    /* Full-width input fields */
    .form-container input[type=text],
    .form-container input[type=password] {
      width: 100%;
      padding: 15px;
      margin: 5px 0 22px 0;
      border: none;
      background: #f1f1f1;
    }

    /* When the inputs get focus, do something */
    .form-container input[type=text]:focus,
    .form-container input[type=password]:focus {
      background-color: #ddd;
      outline: none;
    }

    /* Set a style for the submit/login button */
    .form-container .btn {
      background-color: #4CAF50;
      color: white;
      padding: 16px 20px;
      border: none;
      cursor: pointer;
      width: 100%;
      margin-bottom: 10px;
      opacity: 0.8;
    }

    /* Add a red background color to the cancel button */
    .form-container .cancel {
      background-color: red;
    }

    /* Add some hover effects to buttons */
    .form-container .btn:hover,
    .open-button:hover {
      opacity: 1;
    }
  </style>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow"></nav>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">الرحلات التى ترسل للادارة</h1>
            <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
          </div>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"></h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>الموقع من</th>
                      <th>الموقع الى</th>
                      <th>الحالة</th>
                      <th>التاريخ</th>
                      <th>ملاحظات</th>
                      <th>ملاحظات الادمن</th>
                      <th>معلومات عن المستخدم</th>
                    </tr>
                  </thead>
                  <!-- <tfoot>
                    <tr>
                      <th>Name</th>
                      <th>Position</th>
                      <th>Office</th>
                      <th>Age</th>
                      <th>Start date</th>
                      <th>Salary</th>
                    </tr>
                  </tfoot> -->
                  <tbody>
                    <?php echo $resStr ?>
                  </tbody>

                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- myform -->
      <div class="form-popup" id="myForm">
        <form action="" method="post" class="form-container">
          <h1>ملاحظة</h1>
          <input type="text" id="trip_id" name="trip_id" hidden>
          <input type="text" placeholder="اكتب الملاحظات" name="admin_note" required>

          <button type="submit" name="save_note" class="btn">حفظ</button>
          <button type="submit" class="btn cancel" onclick="closeForm()">اغلاق</button>
        </form>
      </div>

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; El-Farag 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>


  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>

  <script>
    $(document).ready(function() {
      document.getElementById("dataTable_filter").remove();
    });

    function openForm(val) {
      document.getElementById("trip_id").value = val;
      document.getElementById("myForm").style.display = "block";
    }

    function closeForm() {
      document.getElementById("myForm").style.display = "none";
    }
  </script>

</body>

</html>