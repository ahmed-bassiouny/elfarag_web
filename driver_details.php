<?php
require_once "check_auth.php";

// Include config file
require_once "config.php";

$id = $_GET['id'];
$sql = "SELECT * FROM user WHERE firebase_key='$id'";
$result = mysqli_query($link, $sql);
$user = mysqli_fetch_assoc($result);

$resStr = "";



$resStr .= '<tr>';
$resStr .= '<td>الاسم</td>';
$resStr .= '<td>' . $user['name'] . '</td>';
$resStr .= '</tr>';

$resStr .= '<tr>';
$resStr .= '<td>الهاتف</td>';
$resStr .= '<td>' . $user['phone'] . '</td>';
$resStr .= '</tr>';

$resStr .= '<tr>';
$resStr .= '<td>البريد الالكترونى</td>';
$resStr .= '<td>' . $user['email'] . '</td>';
$resStr .= '</tr>';

$resStr .= '<tr>';
$resStr .= '<td>موديل السيارة</td>';
$resStr .= '<td>' . $user['car_model'] . '</td>';
$resStr .= '</tr>';

$resStr .= '<tr>';
$resStr .= '<td> رقم السيارة</td>';
$resStr .= '<td>' . $user['car_number'] . '</td>';
$resStr .= '</tr>';

$resStr .= '<tr>';
$resStr .= '<td> نوع السيارة</td>';
$resStr .= '<td>' . $user['car_type'] . '</td>';
$resStr .= '</tr>';

$resStr .= '<tr>';
$resStr .= '<td>المدينة</td>';
$resStr .= '<td>' . $user['city'] . '</td>';
$resStr .= '</tr>';

$resStr .= '<tr>';
$resStr .= '<td> رقم ايبان</td>';
$resStr .= '<td>' . $user['iban_number'] . '</td>';
$resStr .= '</tr>';

$resStr .= '<tr>';
$resStr .= '<td>  الرقم الشخصى</td>';
$resStr .= '<td>' . $user['personal_number'] . '</td>';
$resStr .= '</tr>';

$resStr .= '<tr>';
$resStr .= '<td>صورة رخصة العربية</td>';
$resStr .= '<td> <img onclick="show(this.src)" width="300pt" height="300pt" src = "' . $user['car_license_url'] . '"/></td>';
$resStr .= '</tr>';

$resStr .= '<tr>';
$resStr .= '<td>صورة رخصة السائق</td>';
$resStr .= '<td> <img onclick="show(this.src)" width="300pt" height="300pt" src = "' . $user['driver_license_url'] . '"/></td>';
$resStr .= '</tr>';

$resStr .= '<tr>';
$resStr .= '<td>صورة التامين</td>';
$resStr .= '<td> <img onclick="show(this.src)" width="300pt" height="300pt" src = "' . $user['insurance_url'] . '"/></td>';
$resStr .= '</tr>';

$resStr .= '<tr>';
$resStr .= '<td>صورة رقم التعريف</td>';
$resStr .= '<td> <img onclick="show(this.src)" width="300pt" height="300pt" src = "' . $user['personal_number_url'] . '"/></td>';
$resStr .= '</tr>';

// photo
$photo = $user["photo"];

// block button

if ($user['block'] == 1) {
  $block_button = '<tr><td>الحالة</td><td><button class = "bg-success text-white"  type="submit"  name="unblock">ازاله البلوك</td></tr>';
} else {
  $block_button = '<tr><td>الحالة</td><td><button class = "bg-danger text-white"  type="submit"  name="block">بلوك</td></tr>';
}

if ($user['access'] == 1) {
  $access_button = '<tr><td>استخدام التطبيق</td><td><button class = "bg-danger text-white"  type="submit"  name="remove_access">تغيير الى غير مسموح له بالدخول</td></tr>';
} else {
  $access_button = '<tr><td>استخدام التطيبق</td><td><button class = "bg-success text-white"  type="submit"  name="access">تغيير الى مسموح له بالدخول</td></tr>';
}



if (isset($_POST['unblock'])) {
  $query1 = "UPDATE user SET block=0 WHERE firebase_key='$id'";
  if (mysqli_query($link, $query1)) {
    echo "<script>
             window.history.go(-1);
     </script>";
  } else {
    echo mysqli_error($link);
  }
}


if (isset($_POST['block'])) {
  $query1 = "UPDATE user SET block=1 WHERE firebase_key='$id'";
  if (mysqli_query($link, $query1)) {
    echo "<script>
             window.history.go(-1);
     </script>";
  } else {
    echo mysqli_error($link);
  }
}

if (isset($_POST['remove_access'])) {
  $query1 = "UPDATE user SET access=0 WHERE firebase_key='$id'";
  if (mysqli_query($link, $query1)) {
    echo "<script>
             window.history.go(-1);
     </script>";
  } else {
    echo mysqli_error($link);
  }
}

if (isset($_POST['access'])) {
  $query1 = "UPDATE user SET access=1 WHERE firebase_key='$id'";
  if (mysqli_query($link, $query1)) {
    echo "<script>
             window.history.go(-1);
     </script>";
  } else {
    echo mysqli_error($link);
  }
}
?>

<!DOCTYPE html>
<html lang="ar" dir='rtl'>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>تفاصيل السائق </title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <style>
    #mask {
      position: fixed;
      left: 0;
      top: 0;
      bottom: 0;
      right: 0;
      margin: auto;
      visibility: hidden;
      z-index: -2;
      background: #000;
      background: rgba(0, 0, 0, 0.8);
      overflow: hidden;
      opacity: 0;
      transition: all .5s ease-in-out;
    }

    #mask.showing {
      opacity: 1;
      z-index: 9000;
      visibility: visible;
      overflow: auto;
      transition: all .5s ease-in-out;
    }

    #boxes {
      display: table;
      width: 100%;
      height: 100%;
    }

    .window {
      max-width: 780px;
      z-index: 9999;
      padding: 20px;
      border-radius: 15px;
      text-align: center;
      margin: auto;
      background-color: #ffffff;
      font-family: 'Segoe UI Light', sans-serif;
      font-size: 15pt;
    }

    .window img {
      width: 100%;
      height: auto;
    }

    .inner {
      display: table-cell;
      vertical-align: middle;
    }

    #popupfoot {
      font-size: 16pt;
    }

    .showImage {
      margin: 0 0 3em;
      display: table;
      text-align: center
    }

    .showImage img {
      display: block
    }
  </style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow"> </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800"> تفاصيل السائق</h1>
            <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
          </div>
          <!-- Page Heading -->

        </div>

        <img width="300pt" height="300pt" style="display: block; margin-left: auto;margin-right: auto; " src=<?php echo $photo ?> />
        <br />
        <table class="table table-bordered dataTable" style="width: 50%">
          <?php echo $resStr ?>
            <form name="reg-frm" action="" method="POST">
              <?php echo $block_button ?>
              <?php echo $access_button ?>
            </form>
        </table>

        <!-- <div > <a class="showImage"><img onclick="show(this.src)" src="http://wallpapercave.com/wp/l5FBhgU.jpg" width="200" height="200"></a> </div> -->
        <div id="mask">
          <div id="boxes">
            <div class="inner">
              <div id="dialog" class="window"> <a href="#" class="close">CLOSE</a>
                <div id="popupfoot"> <img src="#" class="image" alt="Loading..."></img> </div>
              </div>
            </div>
          </div>
        </div>

        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; El-Farag 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>


  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>


  <script>
    

    function show(path) {
      
      $('.image').attr({
          'src': path
        });

        //if close button is clicked
        $('.window .close').click(function(e) {
          //Cancel the link behavior
          e.preventDefault();

          $('#mask').removeClass('showing');
        });

        $('#mask').addClass('showing');
    }
  </script>

</body>

</html>